/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 14:11:31 by alam              #+#    #+#             */
/*   Updated: 2017/08/16 15:50:55 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	stack_push(int data, t_stack *stack)
{
	if (stack->top >= stack->size)
		return ;
	stack->content[stack->top] = data;
	stack->top++;
	if (data > stack->max)
		stack->max = data;
	if (data < stack->min)
		stack->min = data;
}

int		stack_pop(t_stack *stack)
{
	int data;

	if (stack->top == 0)
		return (-42);
	data = stack->content[stack->top - 1];
	stack->content[stack->top - 1] = 0;
	stack->top -= 1;
	if (data >= stack->max)
		stack->max = get_max(*stack);
	if (data <= stack->min)
		stack->min = get_min(*stack);
	return (data);
}

t_stack	new_stack(size_t size)
{
	t_stack	new;

	new.content = ft_memalloc(sizeof(int) * size);
	if (new.content == NULL)
	{
		ft_printf("Error: Failed allocating memory for new stack");
		exit(0);
	}
	new.top = 0;
	new.size = size;
	new.max = INT_MIN;
	new.min = INT_MAX;
	return (new);
}

t_stack	copy_stack(t_stack s)
{
	unsigned int	i;
	t_stack			new;

	i = 0;
	new = s;
	new.content = ft_memalloc(sizeof(int) * s.size);
	while (i < s.size)
	{
		new.content[i] = s.content[i];
		i++;
	}
	return (new);
}

int	find_dupe(t_stack s)
{
	unsigned int	i;
	unsigned int	j;
	int				num;

	j = 0;
	while (j < s.top)
	{
		num = s.content[j];
		i = 0;
		while (i < s.top)
		{
			if (i == j)
			{
				i++;
				continue ;
			}
			if (s.content[i] == num)
				return (1);
			i++;
		}
		j++;
	}
	return (0);
}
