/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 13:46:56 by alam              #+#    #+#             */
/*   Updated: 2017/08/18 18:31:59 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <stdlib.h>
# include "libft/libft.h"
# include <limits.h>
# include <unistd.h>

#include <stdio.h>

# define STDIN 0
# define STDOUT 1
# define STDERR 2

# define PS_CMD_NUM 11

// # define DEBUG_DELAY_US 500000

# define DEBUG_DELAY_US 0

# define UP 0
# define DOWN 1

/*
**   ^ ra
**
**   v rra
*/

typedef struct	s_stack
{
	int				*content;
	unsigned int	top;
	size_t			size;
	int				max;
	int				min;
}				t_stack;

typedef struct	s_node
{
	int				data;
	struct s_node	*s_node;
}				t_node;

typedef struct	s_queue
{
	t_node				*head;
	unsigned int	top;
	size_t			size;
}				t_queue;

typedef struct	s_stats
{
	float	mean;
	float	std_dev;
}				t_stats;

typedef struct	s_rot
{
	int a_rot;
	int	b_rot;

	int	a_dir;
	int	b_dir;
}				t_rot;

typedef int		(*cmd_function)(t_stack *, t_stack *);

void	print_stacks_with_move(t_stack *a, t_stack *b);


/*
**				-----------------
**				|	checker.c	|
**				-----------------
*/
void	exit_program(char *exit_reason);
void	print_stacks(t_stack *a, t_stack *b);

/*
**				---------------------
**				|	checker_input.c	|
**				---------------------
*/
int				check_command(char *input);
int				is_number(char *s);
int				count_args(char **args);

/*
**				---------------------
**				|	load_input.c	|
**				---------------------
*/
void			exit_program(char *exit_reason);
int				load_args(int argc, char **argv, t_stack *a, t_stack *b);
cmd_function	*load_functions(void);

/*
**				-------------------------
**				|	stat_functions.c	|
**				-------------------------
*/
float	fsqrt(float num);
float	get_mean(int *num, int size);
float	get_std_dev(int *num, int size, int mean);
int	min(int a, int b);
int	max(int a, int b);

/*
**				-----------------
**				|	stack.c		|
**				-----------------
*/
void	stack_push(int data, t_stack *stack);
int		stack_pop(t_stack *stack);
t_stack	new_stack(size_t size);
t_stack	copy_stack(t_stack s);
int		find_dupe(t_stack s);

/*
**				---------------------
**				|	stack_check.c	|
**				---------------------
*/
int		stack_is_empty(t_stack *stack);
int		stack_is_full(t_stack *stack);
int		invalid_stack(t_stack *s);
int		stack_is_sorted(t_stack a, int debug);

/*
**				-----------------
**				|	sort.c		|
**				-----------------
*/
// int	should_swap(t_stack s);
int		stack_in_order(t_stack a);
int	put_stack_in_order(t_stack *a, t_stack *b);
int	rotate_down_to_spot(t_stack *a, t_stack *b, int num, int rotations);
int	rotate_up_to_spot(t_stack *a, t_stack *b, int num, int rotations);
int	rotate_stacks(t_stack *a, t_stack *b, t_rot r);
int	sort(t_stack *a, t_stack *b);

/*
**				-----------------
**				|	sort_3.c	|
**				-----------------
*/
int	sort_3(t_stack *a, t_stack *b);

/*
**				---------------------
**				|	sort_small.c	|
**				---------------------
*/
int	sort_small(t_stack *a, t_stack *b);


/*
**				-------------------------
**				|	sort_find_place.c	|
**				-------------------------
*/
int	stack_ascending(t_stack s);
int	find_spot_from_top(t_stack a, t_stack b, long num);
int	find_spot_from_bottom(t_stack a, t_stack b, long num);
int	get_rotation_direction(t_stack a, t_stack b, int index);
t_rot	get_rotation_info(t_stack a, t_stack b, int index);

/*
**				---------------------
**				|	sort_stats.c	|
**				---------------------
*/
int	filter_std_dev(t_stack *a, t_stack *b);

/*
**				-------------------------
**				|	ps_cmd_pushswap.c	|
**				-------------------------
*/
int	sa(t_stack *a, t_stack *b);
int	sb(t_stack *a, t_stack *b);
int	ss(t_stack *a, t_stack *b);
int	pa(t_stack *a, t_stack *b);
int	pb(t_stack *a, t_stack *b);
int	get_max(t_stack s);
int	get_min(t_stack s);

/*
**				---------------------
**				|	ps_cmd_rotate.c	|
**				---------------------
*/
int	ra(t_stack *a, t_stack *b);
int	rb(t_stack *a, t_stack *b);
int	rr(t_stack *a, t_stack *b);

/*
**				-------------------------
**				|	ps_cmd_revrotate.c	|
**				-------------------------
*/
int	rra(t_stack *a, t_stack *b);
int	rrb(t_stack *a, t_stack *b);
int	rrr(t_stack *a, t_stack *b);

/*
**				---------------------
**				|	stack_peek.c	|
**				---------------------
*/
int	peek(t_stack a);
int	peek_below(t_stack a);
int	bottom(t_stack a);

/*
**				---------------------
**				|	stack_find.c	|
**				---------------------
*/
int	find_from_top(t_stack a, int num);
int	find_from_bottom(t_stack a, int num);
int	distance_from_top(t_stack s, unsigned int index);
int	distance_from_bot(t_stack s, unsigned int index);

/*
**				-------------------------
**				|	ps_find_next_move.c	|
**				-------------------------
*/
int		find_shortest_move(int *moves, int size);
int		get_best_move(t_stack a, t_stack b);

#endif
