/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_input.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/26 15:40:02 by alam              #+#    #+#             */
/*   Updated: 2017/08/17 20:21:20 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	exit_program(char *exit_reason)
{
	ft_printf("Error: %s\n", exit_reason);
	exit(0);
}

void			load_loop(t_stack *a, int arg_num, char **args)
{
	int		i;
	long	num;

	i = arg_num - 1;
	while (i >= 0)
	{
		if (is_number(args[i]))
		{
			num = ft_atol(args[i]);
			if (num > INT_MAX || num < INT_MIN)
				exit_program("Number is outside of int bounds");
			stack_push(ft_atoi(args[i]), a);
		}
		else
			exit_program("Input is not a number");
		i--;
	}
	if (find_dupe(*a))
		exit_program("Duplicate value found");
}

int				load_args(int argc, char **argv, t_stack *a, t_stack *b)
{
	int		i;
	int		split_flag;
	int		arg_num;
	int		debug;
	char	**args;

	i = (argc == 2) ? 0 : 1;
	split_flag = (argc == 2) ? 1 : 0;
	args = (argc == 2) ? ft_strsplit(argv[1], ' ') : argv + 1;
	arg_num = (argc == 2) ? count_args(args) : argc - 1;
	if (ft_strncmp(args[0], "-v", 3) == 0)
	{
		debug = 1;
		args = args + 1;
		arg_num--;
	}
	else
		debug = 0;
	// *a = new_stack(arg_num + (split_flag) + debug - (debug * split_flag));
	// *b = new_stack(arg_num + (split_flag) + debug - (debug * split_flag));
	*a = new_stack(arg_num);
	*b = new_stack(arg_num);
	load_loop(a, arg_num, args);
	if (split_flag)
		ft_freesplit(args - debug);
	return (debug);
}

cmd_function	*load_functions(void)
{
	cmd_function *cmds;

	cmds = ft_memalloc(sizeof(cmd_function) * PS_CMD_NUM);
	cmds[0] = &pa;
	cmds[1] = &pb;
	cmds[2] = &sa;
	cmds[3] = &sb;
	cmds[4] = &ss;
	cmds[5] = &ra;
	cmds[6] = &rb;
	cmds[7] = &rr;
	cmds[8] = &rra;
	cmds[9] = &rrb;
	cmds[10] = &rrr;
	return (cmds);
}
