/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_input.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/23 14:06:05 by alam              #+#    #+#             */
/*   Updated: 2017/08/16 15:43:25 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	check_r(char *input)
{
	int out;

	out = -1;
	if (input == NULL || *input == 0)
		return (out);
	if ((input[1] == 'a' || input[1] == 'b') && input[2] == 0)
		out = (input[1] == 'a') ? 5 : 6;
	else if (input[1] == 'r' && input[2] == 0)
		out = 7;
	else if (input[1] == 'r')
	{
		out = check_r(input + 1);
		out = (out != -1) ? out + 3 : -1;
	}
	return (out);
}

int			check_command(char *input)
{
	int	out;

	out = -1;
	if (input == NULL || strlen(input) < 2 || strlen(input) > 3)
		return (-1);
	if (input[0] == 'p')
	{
		if ((input[1] == 'a' || input[1] == 'b') && input[2] == 0)
			out = (input[1] == 'a') ? 0 : 1;
	}
	else if (input[0] == 's')
	{
		if ((input[1] == 'a' || input[1] == 'b') && input[2] == 0)
			out = (input[1] == 'a') ? 2 : 3;
		else if (input[1] == 's')
			out = 4;
	}
	else if (input[0] == 'r')
		out = check_r(input);
	return (out);
}

int				is_number(char *s)
{
	int i;

	i = 0;
	while (s[i] != 0)
	{
		if (!ft_isdigit(s[i]) && s[i] != '-' && s[i] != '+')
			return (0);
		i++;
	}
	return (1);
}

int				count_args(char **args)
{
	int	i;

	i = 0;
	while (args[i] != NULL)
		i++;
	return (i);
}
