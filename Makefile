# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alam <alam@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/10 14:29:56 by alam              #+#    #+#              #
#    Updated: 2017/08/17 19:49:30 by alam             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = push_swap

CHECKER = checker

PS_FILES = push_swap.c stack.c ps_cmd_push.c ps_cmd_swap.c ps_cmd_rotate.c \
			ps_cmd_revrotate.c load_input.c checker_input.c stack_peek.c \
			sort_find_place.c sort.c stat_functions.c sort_stats.c \
			stack_check.c stack_print.c ps_find_next_move.c stack_find.c \
			sort_3.c sort_small.c

CHECKER_FILES = checker.c stack.c ps_cmd_push.c ps_cmd_swap.c ps_cmd_rotate.c \
				ps_cmd_revrotate.c load_input.c checker_input.c stack_peek.c \
				sort.c sort_find_place.c stat_functions.c sort_stats.c \
				stack_check.c stack_print.c ps_find_next_move.c stack_find.c \
				sort_3.c sort_small.c

INCLUDES =  -I libft

LIBRARY =  -L libft -lft

PS_OBJECTS = $(addprefix build/, $(PS_FILES:.c=.o))

CHECKER_OBJECTS = $(addprefix build/, $(CHECKER_FILES:.c=.o))

CFLAGS = -Wall -Wextra -Werror -g -fsanitize=address

all: libft $(CHECKER) $(NAME)

$(NAME):  $(PS_OBJECTS)
	gcc $(CFLAGS) $(INCLUDES) $(LIBRARY) $(FRAMEWORK) -o $(NAME) $(PS_OBJECTS)

$(CHECKER):  $(CHECKER_OBJECTS)
	gcc $(CFLAGS) $(INCLUDES) $(LIBRARY) $(FRAMEWORK) -o $(CHECKER) $(CHECKER_OBJECTS)

libft:
	make -C libft/ all

build/%.o: %.c | build
	gcc $(CFLAGS) $(INCLUDES) -o $@ -c $^

clean:
	rm -rf build/

fclean: clean
	make -C libft/ fclean
	rm -f $(NAME) $(CHECKER)

re: fclean all

new: clean all

build:
	mkdir build/

.PHONY: all clean fclean re libft minilibx
