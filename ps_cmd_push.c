/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_cmd_push.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/30 16:44:53 by alam              #+#    #+#             */
/*   Updated: 2017/08/15 20:36:01 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	pa(t_stack *a, t_stack *b)
{
	if (stack_is_full(a) || stack_is_empty(b))
		return (0);
	stack_push(stack_pop(b), a);
	return (1);
}

int	pb(t_stack *a, t_stack *b)
{
	int	data;

	if (stack_is_full(b) || stack_is_empty(a))
		return (0);
	data = stack_pop(a);
	stack_push(data, b);
	return (1);
}

int	get_max(t_stack s)
{
	unsigned int	i;
	int				max;

	i = 0;
	max = INT_MIN;
	while (i < s.top)
	{
		if (s.content[i] > max)
			max = s.content[i];
		i++;
	}
	return (max);
}

int	get_min(t_stack s)
{
	unsigned int	i;
	int				min;

	i = 0;
	min = INT_MAX;
	while (i < s.top)
	{
		if (s.content[i] < min)
			min = s.content[i];
		i++;
	}
	return (min);
}
