/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_peek.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/26 17:08:21 by alam              #+#    #+#             */
/*   Updated: 2017/08/15 20:16:31 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	peek(t_stack a)
{
	if (stack_is_empty(&a))
		return (INT_MIN);
	return (a.content[a.top - 1]);
}

int	peek_below(t_stack a)
{
	if (a.top < 2)
		return (INT_MIN);
	return (a.content[a.top - 2]);
}

int	bottom(t_stack a)
{
	return (a.content[0]);
}
