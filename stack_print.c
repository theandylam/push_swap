/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_print.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 14:39:09 by alam              #+#    #+#             */
/*   Updated: 2017/08/18 18:23:24 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	print_stacks(t_stack *a, t_stack *b)
{
	int	i;

	i = a->size - 1;
	while (i >= 0)
	{
		if ((unsigned int)i < a->top)
			ft_printf("| % 11d |", a->content[i]);
		else
			ft_printf("|             |");
		ft_printf("       ");
		if ((unsigned int)i < b->top)
			ft_printf("| % 11d |\n", b->content[i]);
		else
			ft_printf("|             |\n");
		i--;
	}
	ft_printf("---------------       ---------------\n");
	ft_printf("|      A      |       |      B      |\n");
}
