/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_small.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/17 16:54:58 by alam              #+#    #+#             */
/*   Updated: 2017/08/18 18:31:31 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	push_to_b(t_stack *a, t_stack *b)
{
	int op_count;

	op_count = 0;
	while(a->top > 3)
	{
		op_count += pb(a, b);
		ft_printf("pb\n");
	}
	return (op_count);
}

int	small_sort(t_stack *a, t_stack *b)
{
	int	op_count;
	int	shortest_move;
	int	push_value;
	t_rot	r;


	op_count = push_to_b(a, b);
	if (a->top == 3)
		op_count += sort_3(a, b);
	while (b->top > 0)
	{
		shortest_move = get_best_move(*a, *b);
		push_value = b->content[shortest_move];
		r = get_rotation_info(*a, *b, shortest_move);
		op_count += rotate_stacks(a, b, r);
		op_count += pa(a, b);
		ft_printf("pa\n");
	}
	op_count += put_stack_in_order(a, b);
	return (op_count);
}

int	sort_small(t_stack *a, t_stack *b)
{
	int op_count;

	op_count = 0;
	if (stack_is_sorted(*a, 0))
		return (0);
	if (a->size == 2)
		op_count = sa(a, b);
	else if (a->size == 3)
		op_count = sort_3(a, b);
	else
		op_count = small_sort(a, b);
	return (op_count);
}
