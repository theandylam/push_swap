/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/17 18:05:49 by alam              #+#    #+#             */
/*   Updated: 2017/08/17 20:25:30 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	min_on_top(t_stack *a, t_stack *b)
{
	int op_count;

	op_count = 0;
	op_count += sa(a, b);
	op_count += ra(a, b);
	ft_printf("sa\nra\n");
	return (op_count);
}

int	mid_on_top(t_stack *a, t_stack *b)
{
	int op_count;

	op_count = 0;
	if (bottom(*a) == a->max)
	{
		op_count = sa(a, b);
		ft_printf("sa\n");
	}
	else
	{
		op_count = rra(a, b);
		ft_printf("rra\n");
	}
	return (op_count);
}

int	max_on_top(t_stack *a, t_stack *b)
{
	int op_count;

	op_count = 0;
	if (bottom(*a) == a->min)
	{
		op_count += ra(a, b);
		op_count += sa(a, b);
		ft_printf("ra\nsa\n");
	}
	else
	{
		op_count += rra(a, b);
		ft_printf("rra\n");
	}
	return (op_count);
}

int	sort_3(t_stack *a, t_stack *b)
{
	int op_count;

	op_count = 0;
	if (stack_ascending(*a))
		return (op_count);
	if (peek(*a) != a->min && peek(*a) != a->max)
		op_count += mid_on_top(a, b);
	else if (peek(*a) == a->max)
		op_count += max_on_top(a, b);
	else
		op_count += min_on_top(a, b);
	return (op_count);
}
