/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/16 14:56:33 by alam              #+#    #+#             */
/*   Updated: 2017/08/16 15:01:49 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int	chkdig(char input)
{
	if ((input >= '0' && input <= '9'))
		return (1);
	else
		return (0);
}

static	int	getoffset(const char *str)
{
	int	i;

	i = 0;
	while ((str[i] <= ' ' && str[i] > 0) || str[i] > '~')
		i++;
	return (i);
}

static	int	checkstrlngth(const char *str, int offset)
{
	int	i;
	int	length;

	length = 0;
	i = offset;
	while ((str[i] != 0 && chkdig(str[i])))
	{
		length++;
		i++;
	}
	return (length);
}

long		ft_atol(const char *str)
{
	int		i;
	int		digit_mult;
	long	output;
	int		offset;

	i = 0;
	digit_mult = 1;
	output = 0;
	offset = 0;
	offset += getoffset(str);
	if (str[offset] == '-' || str[offset] == '+')
		offset++;
	i = checkstrlngth(str, offset) + offset;
	i--;
	while (i >= offset)
	{
		output += (str[i] - 48) * digit_mult;
		digit_mult *= 10;
		i--;
	}
	if (str[offset - 1] == '-')
		output *= -1;
	return (output);
}

