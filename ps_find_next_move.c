/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_find_next_move.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 14:40:34 by alam              #+#    #+#             */
/*   Updated: 2017/08/18 19:39:18 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		find_shortest_move(int *moves, int size)
{
	int	i;
	int	min;
	int	min_index;

	if (moves == NULL || size < 1)
		return (-1);
	i = 0;
	min = INT_MAX;
	while (i < size)
	{
		if (moves[i] <= min)
		{
			min = moves[i];
			min_index = i;
		}
		i++;
	}
	return (min_index);
}

int		find_number_of_moves(t_stack a, t_stack b, int i)
{
	int	moves;
	int	both_up;
	int	both_down;
	int	a_up_b_down;
	int	a_down_b_up;

	both_up = max(find_spot_from_top(a, b, b.content[i]),
					distance_from_top(b, i));
	both_down = max(find_spot_from_bottom(a, b, b.content[i]),
					distance_from_bot(b, i));
	a_up_b_down = find_spot_from_top(a, b, b.content[i])
					+ distance_from_bot(b, i);
	a_down_b_up = find_spot_from_bottom(a, b, b.content[i])
					+ distance_from_top(b, i);
	moves = min(min(both_up, both_down), min(a_up_b_down, a_down_b_up));
	return (moves);
}

int		get_best_move(t_stack a, t_stack b)
{
	unsigned int	i;
	int				*moves;
	int				top_dist;
	int				bot_dist;
	int				shortest_move;

	i = 0;
	top_dist = 0;
	bot_dist = 0;
	moves = ft_memalloc(sizeof(int) * b.top);
	while (i < b.top)
	{
		moves[i] = find_number_of_moves(a, b, i);
		i++;
	}
	shortest_move = find_shortest_move(moves, b.top);
	free(moves);
	return (shortest_move);
}
