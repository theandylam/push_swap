/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 13:41:22 by alam              #+#    #+#             */
/*   Updated: 2017/08/18 19:42:48 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	print_debug(t_stack *a, t_stack *b, char *cmd)
{
	ft_printf("Command: %s\n", cmd);
	print_stacks(a, b);
	ft_printf("\n");
	if (DEBUG_DELAY_US > 0)
		usleep(DEBUG_DELAY_US);
}

int		input_read_loop(t_stack *a, t_stack *b, int debug)
{
	int				cmd_id;
	int				cmd_count;
	char			*line;
	cmd_function	*ps_cmd;

	ps_cmd = load_functions();
	cmd_count = 0;
	while (get_next_line(STDIN, &line) > 0)
	{
		if (line == NULL || line[0] == 0)
			exit_program("Couldn't read command");
		cmd_id = check_command(line);
		if (cmd_id < PS_CMD_NUM && cmd_id >= 0)
		{
			ps_cmd[cmd_id](a, b);
			if (debug)
				print_debug(a, b, line);
			cmd_count++;
		}
		else
			exit_program("Invalid command");
		free(line);
		line = NULL;
	}
	return (cmd_count);
}

int		main(int argc, char **argv)
{
	int		cmd_count;
	int		debug;
	t_stack	a;
	t_stack	b;

	if (argc < 2)
		exit(0);
	debug = load_args(argc, argv, &a, &b);
	if (debug)
		print_stacks(&a, &b);
	cmd_count = input_read_loop(&a, &b, debug);
	if (stack_is_sorted(a, debug))
		ft_printf("OK\n");
	else
		ft_printf("KO\n");
	ft_printf("Stack size: %zu\n", a.size);
	ft_printf("Number of commands: %d\n", cmd_count);
	return (0);
}
