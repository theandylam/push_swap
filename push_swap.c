/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/26 15:21:45 by alam              #+#    #+#             */
/*   Updated: 2017/08/18 18:33:13 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		main(int argc, char **argv)
{
	int		op_count;
	t_stack	a;
	t_stack	b;

	if (argc < 2)
		exit(0);
	load_args(argc, argv, &a, &b);
	op_count = 0;
	if (a.size < 2)
		return (1);
	if (a.size <= 100)
		op_count = sort_small(&a, &b);
	else
		op_count = sort(&a, &b);
	return (0);
}
