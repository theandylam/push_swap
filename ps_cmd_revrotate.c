/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_cmd_revrotate.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 15:13:33 by alam              #+#    #+#             */
/*   Updated: 2017/08/15 20:27:15 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	rra(t_stack *a, t_stack *b)
{
	unsigned int	i;
	int				temp;

	if (a->top < 2 || invalid_stack(a) || invalid_stack(b))
		return (0);
	i = 0;
	temp = a->content[0];
	while (i < a->top - 1)
	{
		a->content[i] = a->content[i + 1];
		i++;
	}
	a->content[i] = temp;
	return (1);
}

int	rrb(t_stack *a, t_stack *b)
{
	unsigned int	i;
	int				temp;

	if (b->top < 2 || invalid_stack(a) || invalid_stack(b))
		return (0);
	i = 0;
	temp = b->content[0];
	while (i < b->top - 1)
	{
		b->content[i] = b->content[i + 1];
		i++;
	}
	b->content[i] = temp;
	return (1);
}

int	rrr(t_stack *a, t_stack *b)
{
	if (a->top < 2 || invalid_stack(a) || invalid_stack(b))
		return (0);
	rra(a, b);
	rrb(a, b);
	return (1);
}
