/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_find_place.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/08 19:00:12 by alam              #+#    #+#             */
/*   Updated: 2017/08/18 17:04:52 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	stack_ascending(t_stack s)
{
	if (peek(s) == s.min && bottom(s) == s.max)
		return (1);
	else
		return (0);
}

int	find_spot_from_top(t_stack target, t_stack source, long num)
{
	int				spot;
	unsigned int	i;
	t_stack			a;
	t_stack			b;

	a = source;
	b = copy_stack(target);
	i = 0;
	spot = -1;
	if (b.top < 2)
		return (0);
	while (i < b.top && spot == -1)
	{
		if (stack_ascending(b) && (num > b.max || num < b.min))
			spot = i;
		else if (stack_ascending(b) && num == b.max)
			spot = i;
		else if (peek(b) > num && bottom(b) <= num)
			spot = i;
		else
			rb(&a, &b);
		i++;
	}
	free(b.content);
	return (spot);
}

int	find_spot_from_bottom(t_stack target, t_stack source, long num)
{
	int				spot;
	unsigned int	i;
	t_stack			a;
	t_stack			b;

	a = source;
	b = copy_stack(target);
	i = 0;
	spot = -1;
	if (b.top < 2)
		return (0);
	while (i < b.top && spot == -1)
	{
		if (stack_ascending(b) && (num > b.max || num < b.min))
			spot = i;
		else if (stack_ascending(b) && num == b.max)
			spot = i;
		else if (peek(b) > num && bottom(b) <= num)
			spot = i;
		else
			rrb(&a, &b);
		i++;
	}
	free(b.content);
	return (spot);
}

int	get_rotation_direction(t_stack a, t_stack b, int index)
{
	int	top_dist;
	int	bot_dist;
	int	direction;

	if (b.top < 2)
	{
		top_dist = find_from_top(b, b.min);
		bot_dist = find_from_bottom(b, b.min);
		direction = (bot_dist < top_dist) ? 0 : 1;
	}
	top_dist = find_spot_from_top(a, b, b.content[index]);
	bot_dist = find_spot_from_bottom(a, b, b.content[index]);
	if (top_dist < distance_from_top(b, index))
		top_dist = distance_from_top(b, index);
	if (bot_dist < distance_from_bot(b, index))
		bot_dist = distance_from_bot(b, index);
	direction = (top_dist < bot_dist) ? 0 : 1;
	return (direction);
}

t_rot	get_rot_dir(int upup, int downdown, int updown, int downup)
{
	int shortest_move;
	t_rot	r;

	shortest_move = min(min(upup, downdown), min(updown, downup));
	if (shortest_move == upup)
	{
		r.a_dir = UP;
		r.b_dir = UP;
	}
	else if (shortest_move == updown)
	{
		r.a_dir = UP;
		r.b_dir = DOWN;
	}
	else if (shortest_move == downup)
	{
		r.a_dir = DOWN;
		r.b_dir = UP;
	}
	else
	{
		r.a_dir = DOWN;
		r.b_dir = DOWN;
	}
	return (r);
}

t_rot	get_rotation_info(t_stack a, t_stack b, int i)
{
	t_rot	r;
	int	both_up;
	int	both_down;
	int	a_up_b_down;
	int	a_down_b_up;

	both_up = max(find_spot_from_top(a, b, b.content[i]), distance_from_top(b, i));
	both_down = max(find_spot_from_bottom(a, b, b.content[i]), distance_from_bot(b, i));

	a_up_b_down = find_spot_from_top(a, b, b.content[i]) + distance_from_bot(b, i);
	a_down_b_up = find_spot_from_bottom(a, b, b.content[i]) + distance_from_top(b, i);

	r = get_rot_dir(both_up, both_down, a_up_b_down, a_down_b_up);
	r.a_rot = (r.a_dir == UP) ? find_spot_from_top(a, b, b.content[i]) : find_spot_from_bottom(a, b, b.content[i]);
	r.b_rot = (r.b_dir == UP) ? distance_from_top(b, i) : distance_from_bot(b, i);
	return (r);
}
