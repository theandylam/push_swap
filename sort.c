/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/04 14:12:51 by alam              #+#    #+#             */
/*   Updated: 2017/08/18 19:36:50 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	put_stack_in_order(t_stack *a, t_stack *b)
{
	int	top_dist;
	int	bot_dist;
	int	op_count;

	op_count = 0;
	top_dist = find_from_top(*a, a->min);
	bot_dist = find_from_bottom(*a, a->min);
	while (!(peek(*a) == a->min && a->content[0] == a->max))
	{
		if (top_dist <= bot_dist)
		{
			op_count += ra(a, b);
			ft_printf("ra\n");
		}
		else
		{
			op_count += rra(a, b);
			ft_printf("rra\n");
		}
		op_count++;
	}
	return (op_count);
}

int	rotate_both(t_stack *a, t_stack *b, t_rot *r)
{
	(r->a_dir == UP) ? rr(a, b) : rrr(a, b);
	(r->a_dir == UP) ? ft_printf("rr\n") : ft_printf("rrr\n");
	r->a_rot--;
	r->b_rot--;
	return (1);
}

int	rotate_one(t_stack *a, t_stack *b, t_rot *r)
{
	int	op_count;

	op_count = 0;
	if (r->a_rot > 0)
	{
		(r->a_dir == UP) ? ra(a, b) : rra(a, b);
		(r->a_dir == UP) ? ft_printf("ra\n") : ft_printf("rra\n");
		r->a_rot--;
		op_count++;
	}
	if (r->b_rot > 0)
	{
		(r->b_dir == UP) ? rb(a, b) : rrb(a, b);
		(r->b_dir == UP) ? ft_printf("rb\n") : ft_printf("rrb\n");
		r->b_rot--;
		op_count++;
	}
	return (op_count);
}

int	rotate_stacks(t_stack *a, t_stack *b, t_rot r)
{
	int	op_count;

	op_count = 0;
	while (r.a_rot > 0 || r.b_rot > 0)
	{
		if (r.a_rot > 0 && r.b_rot > 0 && r.a_dir == r.b_dir)
			op_count += rotate_both(a, b, &r);
		else
			op_count+= rotate_one(a, b, &r);
	}
	return (op_count);
}

int	sort(t_stack *a, t_stack *b)
{
	int	op_count;
	int	shortest_move;
	int	push_value;
	t_rot	r;

	if (stack_is_sorted(*a, 0))
		return (0);
	op_count = filter_std_dev(a, b);
	while (b->top > 0)
	{
		shortest_move = get_best_move(*a, *b);
		push_value = b->content[shortest_move];
		r = get_rotation_info(*a, *b, shortest_move);
		op_count += rotate_stacks(a, b, r);
		op_count += pa(a, b);
		ft_printf("pa\n");
	}
	op_count += put_stack_in_order(a, b);
	return (op_count);
}
