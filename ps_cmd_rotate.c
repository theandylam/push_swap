/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_cmd_rotate.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 15:13:20 by alam              #+#    #+#             */
/*   Updated: 2017/08/14 14:36:52 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	ra(t_stack *a, t_stack *b)
{
	unsigned int	i;
	int				temp;

	if (a->top < 2 || invalid_stack(a) || invalid_stack(b))
		return (0);
	i = a->top - 1;
	temp = a->content[i];
	while (i > 0)
	{
		a->content[i] = a->content[i - 1];
		i--;
	}
	a->content[0] = temp;
	return (1);
}

int	rb(t_stack *a, t_stack *b)
{
	unsigned int	i;
	int				temp;

	if (b->top < 2 || invalid_stack(a) || invalid_stack(b))
		return (0);
	i = b->top - 1;
	temp = b->content[i];
	while (i > 0)
	{
		b->content[i] = b->content[i - 1];
		i--;
	}
	b->content[0] = temp;
	return (1);
}

int	rr(t_stack *a, t_stack *b)
{
	if (invalid_stack(a) || invalid_stack(b))
		return (0);
	ra(a, b);
	rb(a, b);
	return (1);
}
