/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_check.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 14:10:13 by alam              #+#    #+#             */
/*   Updated: 2017/08/18 19:18:27 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		stack_is_empty(t_stack *stack)
{
	if (stack->top == 0)
		return (1);
	else
		return (0);
}

int		stack_is_full(t_stack *stack)
{
	if (stack->top == stack->size)
		return (1);
	else
		return (0);
}

int		invalid_stack(t_stack *s)
{
	if (s->content == NULL || s->top > s->size || s->size == 0)
		return (1);
	else
		return (0);
}

int		stack_is_sorted(t_stack a, int debug)
{
	size_t i;

	i = 0;
	if (a.top != a.size)
	{
		if (debug)
			ft_printf("Stack a is not full\n");
		return (0);
	}
	while (i < a.size - 1)
	{
		if (a.content[i] < a.content[i + 1])
		{
			if (debug)
				ft_printf("Not sorted at i = %zu\n", i);
			return (0);
		}
		i++;
	}
	return (1);
}

int	stack_in_order(t_stack a)
{
	unsigned int i;

	i = 0;
	while (i < a.top)
	{
		if (a.content[i] < a.content[i + 1])
			return (0);
		i++;
	}
	return (1);
}
