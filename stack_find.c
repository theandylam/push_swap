/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_find.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/15 20:12:40 by alam              #+#    #+#             */
/*   Updated: 2017/08/16 19:52:40 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	find_from_top(t_stack a, int num)
{
	unsigned int	i;

	i = 0;
	while (i < a.top)
	{
		if (a.content[a.top - 1 - i] == num)
			return (i);
		i++;
	}
	return (-1);
}

int	find_from_bottom(t_stack a, int num)
{
	unsigned int	i;

	i = 0;
	while (i < a.top)
	{
		if (a.content[i] == num)
			return (i);
		i++;
	}
	return (-1);
}

int	distance_from_top(t_stack s, unsigned int index)
{
	if (index >= s.top)
		return (-1);
	return (s.top - 1 - index);
}

int	distance_from_bot(t_stack s, unsigned int index)
{
	if (index >= s.top)
		return (-1);
	return (index + 1);
}
