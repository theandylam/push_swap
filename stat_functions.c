/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stat_functions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 15:15:50 by alam              #+#    #+#             */
/*   Updated: 2017/08/18 15:52:30 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

float	fsqrt(float num)
{
	float	x0;
	float	xn;
	float	err;

	xn = ft_sqrt(num);
	err = 1;
	while (err > 0.001f)
	{
		x0 = xn;
		xn = 0.5f * (x0 + (num / x0));
		err = xn - x0;
		err = (err < 0) ? err * -1 : err;
	}
	return (xn);
}

float	get_mean(int *num, int size)
{
	int		i;
	float	avg;

	i = 0;
	avg = 0;
	while (i < size)
	{
		avg += num[i];
		i++;
	}
	return (avg / size);
}

float	get_std_dev(int *num, int size, int mean)
{
	int		i;
	float	std_dev;

	i = 0;
	std_dev = 0;
	while (i < size)
	{
		std_dev += (num[i] - mean) * (num[i] - mean);
		i++;
	}
	std_dev = fsqrt(std_dev / size);
	return (std_dev);
}

int	max(int a, int b)
{
	if (a >= b)
		return (a);
	else
		return (b);
}

int	min(int a, int b)
{
	if (a <= b)
		return (a);
	else
		return (b);
}