/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_cmd_swap.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 15:13:11 by alam              #+#    #+#             */
/*   Updated: 2017/08/11 19:31:48 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	sa(t_stack *a, t_stack *b)
{
	int	temp;
	int	top;

	if (a->top < 2 || invalid_stack(a) || invalid_stack(b))
		return (0);
	top = a->top - 1;
	temp = a->content[top];
	a->content[top] = a->content[top - 1];
	a->content[top - 1] = temp;
	return (1);
}

int	sb(t_stack *a, t_stack *b)
{
	int	temp;
	int	top;

	if (b->top < 2 || invalid_stack(a) || invalid_stack(b))
		return (0);
	top = b->top - 1;
	temp = b->content[top];
	b->content[top] = b->content[top - 1];
	b->content[top - 1] = temp;
	return (1);
}

int	ss(t_stack *a, t_stack *b)
{
	int	temp;
	int	top;

	if (invalid_stack(a) || invalid_stack(b))
		return (0);
	top = a->top - 1;
	if (a->top >= 2)
	{
		temp = a->content[top];
		a->content[top] = a->content[top - 1];
		a->content[top - 1] = temp;
	}
	top = b->top - 1;
	if (b->top >= 2)
	{
		temp = b->content[top];
		b->content[top] = b->content[top - 1];
		b->content[top - 1] = temp;
	}
	return (1);
}
