/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_stats.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alam <alam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/11 15:05:32 by alam              #+#    #+#             */
/*   Updated: 2017/08/18 15:37:00 by alam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	should_push(int num, t_stats s, int rep)
{
	// if (rep == 0 && num <= s.mean + s.std_dev && num > s.mean - s.std_dev)
	// 	return (1);
	// else if (rep == 1 && num >= s.mean + s.std_dev)
	// 	return (1);
	// else if (rep == 2 && num <= s.mean - s.std_dev)
	// 	return (1);
	// else
	// 	return (0);

	if (rep == 0 && num <= s.mean + s.std_dev && num >= s.mean)
		return (1);
	else if (rep == 1 && num >= s.mean - s.std_dev && num <= s.mean)
		return (1);
	else if (rep == 2 && num >= s.mean + s.std_dev)
		return (1);
	else if (rep == 3 && num <= s.mean - s.std_dev)
		return (1);
	else
		return (0);
}

int	push_loop(t_stack *a, t_stack *b, int rep, t_stats stats)
{
	int i;
	int op_count;

	i = a->top;
	op_count = 0;
	while (i > 0)
	{
		if (should_push(peek(*a), stats, rep))
		{
			op_count += pb(a, b);
			ft_printf("pb\n");
		}
		else if (peek(*b) > stats.mean - stats.std_dev && rep == 0)
		{
			op_count += rr(a, b);
			ft_printf("rr\n");
		}
		else
		{
			op_count += ra(a, b);
			ft_printf("ra\n");
		}
		i--;
	}
	return (op_count);
}

int	filter_std_dev(t_stack *a, t_stack *b)
{
	int				rep;
	int				op_count;
	t_stats			stats;

	rep = 0;
	op_count = 0;
	stats.mean = get_mean(a->content, a->top);
	stats.std_dev = get_std_dev(a->content, a->top, stats.mean);
	while (rep < 4)
	{
		op_count = push_loop(a, b, rep, stats);
		if (stack_in_order(*a) == 1 || a->top <= 3)
			break ;
		rep++;
	}
	op_count += sort_3(a, b);
	return (op_count);
}
